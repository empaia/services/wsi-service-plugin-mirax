FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.2.3@sha256:9e3b3f7c61f265da8a49a5baa69bc8ce8d425e59e1822e29b1d65cdc27ae8913 AS plugin_build

COPY . /plugin_build
WORKDIR /plugin_build

RUN poetry build

RUN poetry export --dev --without-hashes --output ./dist/dev-requirements.txt

FROM registry.gitlab.com/empaia/services/wsi-service:0.11.15@sha256:2291a0d5f0755647a3ee88e6d285587463c71b9a2bc527bdd22350ac07df7ae6 AS wsi_service_development_plugin_integration

COPY --from=plugin_build /plugin_build/dist/ /plugin_build/dist/

RUN pip3 install --user --no-warn-script-location -r /plugin_build/dist/dev-requirements.txt

RUN pip3 install --user --no-warn-script-location /plugin_build/dist/*.whl

FROM registry.gitlab.com/empaia/services/wsi-service:0.11.15@sha256:2291a0d5f0755647a3ee88e6d285587463c71b9a2bc527bdd22350ac07df7ae6 AS wsi_service_development_plugin_integration

COPY --from=plugin_build /plugin_build/dist/ /plugin_build/dist/

RUN pip3 install --user /plugin_build/dist/*.whl
