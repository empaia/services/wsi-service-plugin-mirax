from io import BytesIO

from fastapi import HTTPException
from PIL import Image
from wsi_service.models.v3.slide import SlideExtent, SlideInfo, SlideLevel, SlidePixelSizeNm
from wsi_service.slide import Slide as BaseSlide

from wsi_service_plugin_mirax.zmq_connection import ZmqConnection


class Slide(BaseSlide):
    slideinfo = None
    label = None
    macro = None
    thumbnail = None

    async def open(self, filepath):
        self.filepath = filepath

        # we need to remove the local data dir from our filename because the local
        # dir is mapped to /data in mirax-backend container
        if filepath.startswith("/data"):
            self.filepath = filepath.replace("/data", "")

        zmq_connection = ZmqConnection()
        req = {"req": "verification", "filepath": self.filepath}

        rep_msg = await zmq_connection.send_and_recv_json(req)

        if rep_msg["status_code"] != 200:
            raise HTTPException(
                status_code=rep_msg["status_code"],
                detail=rep_msg["detail"],
            )

    async def close(self):
        self.filepath = None

    async def get_info(self):
        if self.slideinfo is None:
            zmq_connection = ZmqConnection()
            req = {"req": "get_info", "filepath": self.filepath}
            rep_msg = await zmq_connection.send_and_recv_json(req)
            self.slideinfo = self.__parse_info_reply(rep_msg)
        return self.slideinfo

    async def get_region(self, level, start_x, start_y, size_x, size_y, padding_color=None, z=0):
        zmq_connection = ZmqConnection()
        req = {
            "req": "get_region",
            "filepath": self.filepath,
            "level": level,
            "start_x": start_x,
            "start_y": start_y,
            "size_x": size_x,
            "size_y": size_y,
        }
        data, image_array = await zmq_connection.send_and_recv_multi(req)
        if data["rep"] == "success":
            if data["img_data_width"] == data["req_width"] and data["img_data_height"] == data["req_height"]:
                image = Image.open(BytesIO(image_array))
            else:
                padding_color = padding_color if padding_color is not None else (255, 255, 255)
                image = Image.new("RGB", (data["req_width"], data["req_height"]), color=padding_color)
                tmp_image = Image.open(BytesIO(image_array))
                image.paste(tmp_image)

            return image
        else:
            raise HTTPException(status_code=data["status_code"], detail=data["detail"])

    async def get_thumbnail(self, max_x, max_y):
        if self.thumbnail is None:
            zmq_connection = ZmqConnection()
            req = {"req": "THUMBNAIL", "filepath": self.filepath, "max_x": max_x, "max_y": max_y}
            data, image_array = await zmq_connection.send_and_recv_multi(req)
            if data["rep"] == "error":
                raise HTTPException(status_code=data["status_code"], detail=data["detail"])
            image = Image.open(BytesIO(image_array))
            image.thumbnail((max_x, max_y), resample=Image.ANTIALIAS)
            self.thumbnail = image
        return self.thumbnail

    async def _get_associated_image(self, associated_image_name):
        zmq_connection = ZmqConnection()
        req = {"req": associated_image_name, "filepath": self.filepath}
        data, image_array = await zmq_connection.send_and_recv_multi(req)
        if data["rep"] == "error":
            raise HTTPException(status_code=data["status_code"], detail=data["detail"])
        return Image.open(BytesIO(image_array))

    async def get_label(self):
        if self.label is None:
            self.label = await self._get_associated_image("LABEL")
        return self.label

    async def get_macro(self):
        if self.macro is None:
            self.macro = await self._get_associated_image("MACRO")
        return self.macro

    async def get_tile(self, level, tile_x, tile_y, padding_color=None, z=0):
        zmq_connection = ZmqConnection()
        req = {
            "req": "get_tile",
            "filepath": self.filepath,
            "level": level,
            "tile_x": tile_x,
            "tile_y": tile_y,
        }
        data, image_array = await zmq_connection.send_and_recv_multi(req)
        if data["rep"] == "success":
            if data["img_data_width"] == data["req_width"] and data["img_data_height"] == data["req_height"]:
                image = Image.open(BytesIO(image_array))
            else:
                padding_color = padding_color if padding_color is not None else (255, 255, 255)
                image = Image.new("RGB", (data["req_width"], data["req_height"]), color=padding_color)
                tmp_image = Image.open(BytesIO(image_array))
                image.paste(tmp_image)

            return image
        else:
            raise HTTPException(status_code=data["status_code"], detail=data["detail"])

    # private member
    def __parse_levels(self, info):
        levels = []
        for level in info["levels"]:
            levels.append(
                SlideLevel(
                    extent=SlideExtent(
                        x=level["extent"]["x"],
                        y=level["extent"]["y"],
                        z=level["extent"]["z"],
                    ),
                    downsample_factor=level["downsample_factor"],
                )
            )
        return levels

    def __parse_info_reply(self, info):
        levels = self.__parse_levels(info)

        slide_info_obj = SlideInfo(
            id=info["id"],
            channels=info["channels"],
            channel_depth=info["channel_depth"],
            extent=SlideExtent(x=info["extent"]["x"], y=info["extent"]["y"], z=info["extent"]["z"]),
            pixel_size_nm=SlidePixelSizeNm(x=info["pixel_size_nm"]["x"], y=info["pixel_size_nm"]["y"], z=None),
            tile_extent=SlideExtent(
                x=info["tile_extent"]["x"],
                y=info["tile_extent"]["y"],
                z=info["tile_extent"]["z"],
            ),
            num_levels=len(levels),
            levels=levels,
        )
        return slide_info_obj
