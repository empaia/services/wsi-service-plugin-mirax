import os

from .slide import Slide


def is_supported(filepath):
    if os.path.isfile(filepath):
        return filepath.endswith(".mrxs")
    return False


async def open(filepath):
    return await Slide.create(filepath)
