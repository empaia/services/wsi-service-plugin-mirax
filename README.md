# WSI Service Plugin Mirax

Plugin in for the [WSI-Service](https://gitlab.com/empaia/services/wsi-service) providing brightfield and fluorescence support for **3DHistech Mirax** file format. The plugin is based on the third-party Slide Access Library by 3DHistech. These binaries are solely supported under Windows, which means that the actual backend, calling the underlying programming interface, needs to be build as well under Windows. The plugin and the mirax backend (running in a Windows emulated environment (wine)) are communicating through a simple socket-based messaging protocol (zeromq). The backend repository can be found [here](https://gitlab.com/empaia/integration/mirax-backend).

## Requirements

* Install Visual Studio with .NET Framework 4.72
* Checkout the [Mirax Backend Repository](https://gitlab.com/empaia/integration/mirax-backend) under Windows.
* Follow the build instructions in the README. Short summary:
    * Install the SlideAC / SlideViewer of 3DHistech in 32-bit mode
    * Build in `Release` mode on `x86` architecture

As result, a zip file will be created in the output directory (`/bin/x86/mirax_backend.zip`). This will include all necessary external dependencies and a prebuild version of the backend.

Copy the `mirax_backend.zip` file to your local plugin project structure under `mirax_backend`:

```
wsi-service-plugin-mirax/
├── dist/
├── mirax_backend/
    └── Dockerfile
    └── entrypoint.sh
    └── mirax_backend.zip
├── wsi_service_plugin_mirax/
...
```

## Get started

Make sure [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/) is installed.

Set environment variables in your shell or in a `.env` file:

```bash
# WSI-Service related
WS_CORS_ALLOW_ORIGINS=["*"]
WS_DISABLE_OPENAPI=False
WS_MAPPER_ADDRESS=http://localhost:8080/slides/{slide_id}/storage
WS_LOCAL_MODE=True
WS_INACTIVE_HISTO_IMAGE_TIMEOUT_SECONDS=600
WS_MAX_RETURNED_REGION_SIZE=25000000
WS_ROOT_PATH=
WS_ENABLE_VIEWER_ROUTES=False

# Mirax backend related
WSPM_TCP_ROUTER_PORT=5557
WSPM_CACHE_TTL_SECS=600
WSPM_MAX_CACHE_SLIDE_HANDLES=10
WSPM_CACHE_INVALIDATION_INTERVAL_SECS=60
WSPM_DEFAULT_TILE_EXTENT=-1 # use native tile extent
WSPM_VERBOSE_LOGGING=false

COMPOSE_RESTART=no
COMPOSE_NETWORK=default
COMPOSE_WS_PORT=8080
COMPOSE_DATA_DIR=/data
```

See  WSI Service repository for details on these variables

```bash
docker-compose up --build
```

Afterwards, visit `http://localhost:${COMPOSE_WS_PORT}/docs` (Note: Running on port that is defined as environment variable)

## Development

// TODO

### Run tests

// TODO