# Changelog

## 0.1.22

* mirax backend update

## 0.1.21

* mirax backend update

## 0.1.20

* updated backend (default tile extent) and cache slide data in plugin

## 0.1.19

* fixed pillow < 10.0.0

## 0.1.18

* renovate

## 0.1.16 & 0.1.17

* updated backend

## 0.1.15

* update pip install in Dockerfile

## 0.1.14

* renovate

## 0.1.13

* renovate

## 0.1.12

* updated readme

## 0.1.11

* revert renovate

## 0.1.10

* update mirax backend to version 0.0.6.0
    * worker task for each slide handle
    * sliding expiration time for worker cache
* renovate

## 0.1.9

* added 5s zmq polling timeout
* renovate

## 0.1.8

* fix for thumbnail endpoint

## 0.1.6 & 0.1.7

* bugfix during slide initialization
* raised wsi-service version

## 0.1.5

* bugfixes for plugin and backend code
* peformance improvements in backend
* improved docker file

## 0.1.4

* bugfix for is_supported

## 0.1.3

* renovate

## 0.1.2

* added docker build step in CI

## 0.1.1

* renovate

## 0.1.0

* added priority and is_supported for new plugin mechanism support

## 0.0.2

* pin zmq port to 5557 (todo: make configurable by env var later)

## 0.0.1

* initial implementation
